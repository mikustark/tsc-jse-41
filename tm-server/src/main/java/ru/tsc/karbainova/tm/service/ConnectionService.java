package ru.tsc.karbainova.tm.service;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.eclipse.persistence.exceptions.DatabaseException;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import ru.tsc.karbainova.tm.api.repository.IProjectRepository;
import ru.tsc.karbainova.tm.api.repository.ISessionRepository;
import ru.tsc.karbainova.tm.api.repository.ITaskRepository;
import ru.tsc.karbainova.tm.api.repository.IUserRepository;
import ru.tsc.karbainova.tm.api.service.IConnectionService;
import ru.tsc.karbainova.tm.api.service.IPropertyService;
import ru.tsc.karbainova.tm.dto.Project;
import ru.tsc.karbainova.tm.dto.Session;
import ru.tsc.karbainova.tm.dto.Task;
import ru.tsc.karbainova.tm.dto.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.Map;

public class ConnectionService implements IConnectionService {

    @NonNull
    private final IPropertyService propertyService;

    @NonNull
    private final SqlSessionFactory sqlSessionFactory;

    @NonNull
    private final EntityManagerFactory entityManagerFactory;

    public ConnectionService(@NonNull IPropertyService propertyService) {
        this.propertyService = propertyService;
        sqlSessionFactory = getSqlSessionFactory();
        entityManagerFactory = factory();
    }

    @NonNull
    @Override
    public SqlSession getSqlSession() {
        return sqlSessionFactory.openSession();
    }

    @NonNull
    private SqlSessionFactory getSqlSessionFactory() {
        final String driver = propertyService.getJdbcDriver();
        final String user = propertyService.getJdbcUser();
        final String password = propertyService.getJdbcPassword();
        final String url = propertyService.getJdbcUrl();

        @NonNull DataSource dataSource = new PooledDataSource(driver, url, user, password);
        @NonNull TransactionFactory transactionFactory = new JdbcTransactionFactory();
        @NonNull Environment environment = new Environment("development", transactionFactory, dataSource);
        @NonNull Configuration configuration = new Configuration(environment);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(ISessionRepository.class);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

    @NonNull
    private EntityManagerFactory factory() {
        final String driver = propertyService.getJdbcDriver();
        final String user = propertyService.getJdbcUser();
        final String password = propertyService.getJdbcPassword();
        final String url = propertyService.getJdbcUrl();
        final String dialect = propertyService.getDialect();
        final String auto = propertyService.getAuto();
        final String sqlshow = propertyService.getSqlShow();

        @NonNull Map<String, String> settings = new HashMap<>();
        settings.put(org.hibernate.cfg.Environment.DRIVER, driver);
        settings.put(org.hibernate.cfg.Environment.USER, user);
        settings.put(org.hibernate.cfg.Environment.PASS, password);
        settings.put(org.hibernate.cfg.Environment.URL, url);
        settings.put(org.hibernate.cfg.Environment.DIALECT, dialect);
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, auto);
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, sqlshow);

        @NonNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NonNull final StandardServiceRegistry registry = registryBuilder.build();
        @NonNull final MetadataSources sources = new MetadataSources(registry);

        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Session.class);
        sources.addAnnotatedClass(User.class);

        @NonNull Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }
}
