package ru.tsc.karbainova.tm.command.user;

import ru.tsc.karbainova.tm.command.AbstractCommand;
import ru.tsc.karbainova.tm.command.TerminalUtil;
import ru.tsc.karbainova.tm.endpoint.Session;

public class UserRemoveByLoginCommand extends AbstractCommand {
    @Override
    public String name() {
        return "user-remove-by-login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " Remove user";
    }

    @Override
    public void execute() {
        System.out.println("Enter login");
        final String login = TerminalUtil.nextLine();
        Session session = serviceLocator.getSession();
//        serviceLocator.getAdminUserEndpoint().removeByLoginUser(session, login);
    }

}
